<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Location</title>

    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/location.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    <?php include_once('includes/guest-header.php'); ?>
    <?php include_once('includes/guest-navBar.php'); ?>

    <h3 style="text-align: center;">Visit us at the following locations:</h3>
    <div class="row">
        <div class="col-md-6 mb-4">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header blue-gradient">
                    <h5 class="mb-0">Passeig de Picasso, 21, 08003 Barcelona, Spain</h5>
                </div>
                <div class="card-body card-body-cascade text-center">
                    <div id="map-container-google-8" class="z-depth-1-half map-container-5" style="height: 300px">
                        <iframe src="https://maps.google.com/maps?q=Barcelona&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 mb-4">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header peach-gradient">
                    <h5 class="mb-0">Paseo de la Puerta del Ángel, 1, 28011 Madrid, Spain</h5>
                </div>
                <div class="card-body card-body-cascade text-center">
                    <div id="map-container-google-9" class="z-depth-1-half map-container-5" style="height: 300px">
                        <iframe src="https://maps.google.com/maps?q=Madryt&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>