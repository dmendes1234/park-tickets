<?php
session_start();
include('includes/dbconnection.php');
error_reporting(0);
if (strlen($_SESSION['ptmsaid'] == 0)) {
  header('location:logout.php');
} else {
  if (isset($_POST['submit'])) {
    $tid = $_GET['editid'];
    $tpype = $_POST['tickettype'];
    $tprice = $_POST['tprice'];

    $query = mysqli_query($con, "update tbltickettype set TicketType='$tpype',Price='$tprice' where ID='$tid'");
    if ($query) {

      echo '<script>
      alert("Price successfully changed");
 </script>';
    } else {

      echo '<script>alert("Something Went Wrong. Please try again.")</script>';
    }
  }


?>

  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Ticket Price</title>

    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/br-posjetitelja.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>

  <body>


    <?php include_once('includes/header.php'); ?>
    <?php include_once('includes/navBar.php'); ?>

    <div class="container" style="margin-top: 30px;">
      <h2>Update Ticket Price</h2>
      <form method="post" action="" name="">

        <?php
        $tid = $_GET['editid'];
        $ret = mysqli_query($con, "select * from tbltickettype where ID='$tid'");
        $cnt = 1;
        $row = mysqli_fetch_array($ret);

        ?>
        <div class="form-group">
          <p>Ticket Type</p>
          <input type="text" class="form-control" id="tickettype" name="tickettype" value="<?php echo $row['TicketType']; ?>" required="true" readonly>
        </div>
        <div class="form-group">
          <p>Ticket Cost</p>
          <input type="text" class="form-control" id="tprice" name="tprice" aria-describedby="emailHelp" value="<?php echo $row['Price']; ?>" required="true">
        </div>
      <?php } ?>
      <button type="submit" name="submit" class="btn btn-primary">Update</button>
      </form>
    </div>
  </body>

  </html>