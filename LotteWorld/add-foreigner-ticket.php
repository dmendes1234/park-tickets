<?php
session_start();
include('includes/dbconnection.php');
error_reporting(0);
if (strlen($_SESSION['ptmsaid'] == 0)) {
  header('location:logout.php');
} else {
  if (isset($_POST['submit'])) {
    $noadult = $_POST['noadult'];
    $nochildren = $_POST['nochildren'];
    $aprice = $_POST['aprice'];
    $cprice = $_POST['cprice'];
    $ticketid = mt_rand(100000000, 999999999);

    $query = mysqli_query($con, "insert into  tblticforeigner(TicketID,NoAdult,NoChildren,AdultUnitprice,ChildUnitprice) value('$ticketid','$noadult','$nochildren','$aprice','$cprice')");
    if ($query) {

      echo '<script>alert("Ticket information has been added.")</script>';
    } else {
      echo '<script>alert("Something Went Wrong. Please try again.")</script>';
    }
  }


?>

  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Ticket</title>


    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/br-posjetitelja.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>

  <body>
    <?php include_once('includes/header.php'); ?>
    <?php include_once('includes/navBar.php'); ?>


    <div class="container" style="margin-top: 30px;">
      <h2>Add Foreigner Ticket</h2>
      <form method="post" action="" name="">
        <div class="form-group">
          <label for="exampleInputEmail1">Adult</label>
          <input type="text" class="form-control" id="noadult" name="noadult" aria-describedby="emailHelp" placeholder="No. of Adult" value="" required="true">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Children</label>
          <input type="text" class="form-control" id="nochildren" name="nochildren" aria-describedby="emailHelp" placeholder="No. of Childrens" value="" required="true">

        </div>

        <?php

        $ret = mysqli_query($con, "select * from tbltickettype where TicketType='Foreigner Adult'");
        $cnt = 1;
        $row = mysqli_fetch_array($ret)

        ?>
        <input type="hidden" name="aprice" value="<?php echo $row['Price']; ?>">
        <?php

        $ret = mysqli_query($con, "select * from tbltickettype where TicketType='Foreigner Child'");
        $cnt = 1;
        $row = mysqli_fetch_array($ret)

        ?>
        <input type="hidden" name="cprice" value="<?php echo $row['Price']; ?>">

        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" name="submit">Submit</button>
      </form>
      </form>
    </div>
  </body>

  </html>
<?php } ?>