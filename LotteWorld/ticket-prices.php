<?php
include('includes/dbconnection.php');
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Park Tickets || Manage Ticket</title>

        <link rel="stylesheet" href="css/main.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>

    <body>
        <?php include_once('includes/guest-header.php'); ?>
        <?php include_once('includes/guest-navBar.php'); ?>
        <div class="container-fluid" style="margin-top: 50px;">
            <h2>Ticket Prices</h2>

            <table class="table table-striped">
                <tr>
                    <th>S.NO</th>
                    <th>Ticket Type</th>
                    <th>Price $</th>
                </tr>
                <?php
                $ret = mysqli_query($con, "select * from tbltickettype");
                $cnt = 1;
                while ($row = mysqli_fetch_array($ret)) {

                ?>
                    <tr>
                        <td><?php echo $cnt; ?></td>
                        <td><?php echo $row['TicketType']; ?></td>
                        <td><?php echo $row['Price']; ?></td>
                    </tr>
                <?php
                    $cnt = $cnt + 1;
                } ?>
            </table>
        </div>
        
    </body>
    </html>

