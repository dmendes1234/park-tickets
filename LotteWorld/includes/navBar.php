<nav class="navbar navbar-expand-md sticky-top bg-dark navbar-dark w-100">
    <a class="navbar-brand" href="dashboard.php">HOME</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="manage-ticket.php">Manage Ticket Prices</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Normal Ticket
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="add-normal-ticket.php">Add Ticket</a>
                    <a class="dropdown-item" href="manage-normal-ticket.php">Manage Ticket</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Foreigner Ticket
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="add-foreigner-ticket.php">Add Ticket</a>
                    <a class="dropdown-item" href="manage-foreigner-ticket.php">Manage Ticket</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Reports
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="normal-people-report.php">Normal People Report</a>
                    <a class="dropdown-item" href="foreigner-people-report.php">Foreigner People Report</a>
                </div>
            </li>


            <form class="form-inline" action="search.php" style="margin-left:10px;" method="POST">
                <input class="form-control mr-sm-2" type="text" placeholder="Ticket ID" name="searchdata" style="width: 250px;">
                <button class="btn btn-info" type="submit" name="search">Search</button>
            </form>
        </ul>
    </div>
</nav>
<?php include_once('includes/progressBar.php'); ?>
