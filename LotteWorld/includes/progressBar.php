<div id="myProgress" title="Broj posjetitelja">
  <div id="myBar">10</div>
</div>

<script>
    var br_posjetitelja = Math.floor(Math.random() * 100);
    document.getElementById("myBar").style.width = br_posjetitelja + "%";
    if(br_posjetitelja > 6){
    document.getElementById("myBar").innerHTML = "Visitors: " + br_posjetitelja + ' <i class="fa fa-users" aria-hidden="true"></i>';
    }else{
        document.getElementById("myBar").innerHTML = br_posjetitelja;
    }

    if(br_posjetitelja > 65 && br_posjetitelja < 80 ){
        document.getElementById("myBar").style.background = "orange";
    }
    else if(br_posjetitelja >= 80){
        document.getElementById("myBar").style.background = "red";
    }
</script>