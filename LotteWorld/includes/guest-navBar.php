<nav class="navbar navbar-expand-md bg-dark navbar-dark w-100">
        <a class="navbar-brand" href="guest-dashboard.php">NEWS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="ticket-prices.php">Prices</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="park-location.php">Location</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="about-us.php">About us</a>
                </li>
            </ul>
        </div>
        <div class="float-right">
        <button class="btn btn-primary" type="button" style="background-color: gray; border-color:brown"><a href="index.php" style="color: white;">Log in</a></button>
        </div>
    </nav>
