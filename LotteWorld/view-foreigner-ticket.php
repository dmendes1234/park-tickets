<?php
session_start();
include('includes/dbconnection.php');
error_reporting(0);
if (strlen($_SESSION['ptmsaid'] == 0)) {
    header('location:logout.php');
} else {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>View Foreigner Ticket</title>

        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/br-posjetitelja.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>
        <?php include_once('includes/header.php'); ?>
        <?php include_once('includes/navBar.php'); ?>

        <?php
        $vid = $_GET['viewid'];
        $ret = mysqli_query($con, "select * from tblticforeigner where ID='$vid'");
        $cnt = 1;
        while ($row = mysqli_fetch_array($ret)) {

        ?>

            <div class="container" style="margin-top: 50px;" id="printforeigner">
                <h4 class="header-title" style="color: blue">View Detail of Ticket ID: <?php echo $row['TicketID']; ?></h4>
                <h5 class="header-title" style="color: blue">Visiting Date: <?php echo $row['PostingDate']; ?></h5>
                <table class="table table-striped">
                    <tr>
                        <th>#</th>
                        <th>No of Tickets</th>
                        <th>Price per unit</th>
                        <th>Total</th>
                    </tr>

                    <tr>
                        <th>Number of Adult</th>
                        <td style="padding-left: 10px;"><?php echo $noadult = $row['NoAdult']; ?></td>
                        <td style="padding-left: 10px">$<?php echo $cup = $row['AdultUnitprice']; ?></td>
                        <td style="padding-left: 10px">$<?php echo $ta = $cup * $noadult; ?></td>
                    </tr>

                    <tr>
                        <th>Number of Children</th>
                        <td style="padding-left: 10px"><?php echo $nochild = $row['NoChildren']; ?></td>
                        <td style="padding-left: 10px">$<?php echo $aup = $row['ChildUnitprice']; ?></td>
                        <td style="padding-left: 10px">$<?php echo $tc = $aup * $nochild; ?></td>
                    </tr>

                    <tr>
                        <th style="text-align: center;color: red;font-size: 20px; padding-left:100px" colspan="3">Total Ticket Price</th>
                        <td style="padding-left: 10px; color:red">$<?php echo ($ta + $tc); ?></td>
                    </tr>
                </table>
                <p style="margin-top:1%; text-align:center">
                    <i class="fa fa-print fa-2x" style="cursor: pointer;" OnClick="CallPrint(this.value)"></i>
                </p>
            </div>
            <script>
                function CallPrint(strid) {
                    var prtContent = document.getElementById("printforeigner");
                    var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
                    WinPrint.document.write(prtContent.innerHTML);
                    WinPrint.document.close();
                    WinPrint.focus();
                    WinPrint.print();
                    WinPrint.close();
                }
            </script>
    </body>

    </html>
<?php }  ?>
<?php }  ?>