<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['ptmsaid'] == 0)) {
    header('location:logout.php');
} else {
    if (isset($_POST['addArticle'])) {
        $title = $_POST['title'];
        $text = $_POST['text'];
        $article_image = $_POST['image'];
        $author = $_POST['author'];
        $author_image = $_POST['author-image'];
        $date = $_POST['date'];

        $query = mysqli_query($con, 'insert into tblarticle (Title, Text, ArticleImage, Author, AuthorImage, Date) values ("' . $title . '", "' . $text . '", "' . $article_image . '", "' . $author . '", "' . $author_image . '", "' . $date . '")');
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Park Tickets || Dashboard</title>

    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/br-posjetitelja.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        i:hover {
            color: greenyellow;
            cursor: pointer;
        }

        #title:hover{
            color: red;
        }

    </style>

</head>

<body>
    <?php include_once('includes/header.php'); ?>
    <?php include_once('includes/navBar.php'); ?>

    <?php
    $ret = mysqli_query($con, "select * from tblarticle");
    while ($row = mysqli_fetch_array($ret)) {
    ?>
        <div class="container mt-5 mb-5">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="d-flex flex-row"></div>
                    <div class="row news-card p-3 bg-white">
                        <div class="col-md-4">
                            <div class="feed-image"><img class="news-feed-image rounded img-fluid img-responsive" src="<?php echo $row['ArticleImage']; ?>"></div>
                        </div>
                        <div class="col-md-8">
                            <div class="news-feed-text">
                                <h5><?php echo $row['Title']; ?></h5><span><?php echo $row['Text']; ?><br></span>
                                <div class="d-flex flex-row justify-content-between align-items-center mt-2">
                                    <div class="d-flex creator-profile"><img class="rounded-circle" src="<?php echo $row['AuthorImage']; ?>" width="50" height="50">
                                        <div class="d-flex flex-column ml-2">
                                            <h6 class="username"><?php echo $row['Author']; ?></h6><span class="date"><?php echo $row['Date']; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="container mt-5 mb-5">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="d-flex flex-row"></div>
                    <div class="row news-card p-3 bg-white">
                        <div class="col-md-4">
                            <div class="feed-image"><img class="news-feed-image rounded img-fluid img-responsive" src="https://www.who.int/images/default-source/health-topics/health-financing/novelcoronavirus-optimized.jpg?sfvrsn=755458c4_12"></div>
                        </div>
                        <div class="col-md-8">
                            <div class="news-feed-text">
                                <h5>Changes in working hours during coronavirus</h5><span>New working time of park will be by following schedule:
                                    <ul>
                                        <li>Monday-Friday: 8AM - 7PM </li>
                                        <li>Weekend: 9AM - 10PM </li>
                                    </ul>
                                </span>
                                <div class="d-flex flex-row justify-content-between align-items-center mt-2">
                                    <div class="d-flex creator-profile"><img class="rounded-circle" src="https://i.imgur.com/uSlStch.jpg" width="50" height="50">
                                        <div class="d-flex flex-column ml-2">
                                            <h6 class="username">Andrej Bozinovic</h6><span class="date">Jan 20,2020</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mt-5 mb-5">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8">
                        <div class="d-flex flex-row"></div>
                        <div class="row news-card p-3 bg-white">
                            <div class="col-md-4">
                                <div class="feed-image"><img class="news-feed-image rounded img-fluid img-responsive" src="https://talkroute.com/wp-content/uploads/2016/09/price-too-low-862x862.jpg"></div>
                            </div>
                            <div class="col-md-8">
                                <div class="news-feed-text">
                                    <h5>New ticket prices</h5><span>Check the new prices in section "Prices". Price changes are applied to following categories: normal adult and foreigner child. Stay safe!<br></span>
                                    <div class="d-flex flex-row justify-content-between align-items-center mt-2">
                                        <div class="d-flex creator-profile"><img class="rounded-circle" src="https://static.jobscan.co/blog/uploads/linkedin-profile-picture-1280x720.jpg" width="50" height="50">
                                            <div class="d-flex flex-column ml-2">
                                                <h6 class="username">Sanja Dolic</h6><span class="date">Jan 20,2020</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mt-5 mb-5">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-8">
                            <div class="d-flex flex-row"></div>
                            <div class="row news-card p-3 bg-white">
                                <div class="col-md-4">
                                    <div class="feed-image"><img class="news-feed-image rounded img-fluid img-responsive" src="https://pyxis.nymag.com/v1/imgs/225/458/4497f94e5986f7f1526b839772c3fcbfae-japan-roller-coaster.rsquare.w1200.jpg"></div>
                                </div>
                                <div class="col-md-8">
                                    <div class="news-feed-text">
                                        <h5>New Roller Coaster: "Pitaurus 300 Pro Max"</h5><span>The new machines coming to our Park. After sensational Armagedon Sl 3000, we are now introducing you the new "Pitaurus 300 Pro Max". Double the size, double the track, double the fun. What do you waiting for? Come and visit as! Happy playing :)<br></span>
                                        <div class="d-flex flex-row justify-content-between align-items-center mt-2">
                                            <div class="d-flex creator-profile"><img class="rounded-circle" src="https://i.imgur.com/uSlStch.jpg" width="50" height="50">
                                                <div class="d-flex flex-column ml-2">
                                                    <h6 class="username">Andrej Bozinovic</h6><span class="date">Jan 20,2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <i style="font-size:48px; margin:auto; display: inline-block; width: 100%; text-align:center; margin-top:30px" class="fa fa-plus" aria-hidden="true" title="Add new article" data-toggle="modal" data-target="#myModal">
                        Add new article</i>

                    <div class="container">
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-xl">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Add new article</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="container">
                                            <h4>Enter article details</h4>
                                            <form method="POST">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" class="form-control" id="title" placeholder="Enter article title" name="title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Text</label>
                                                    <textarea class="form-control" id="text" placeholder="Enter article text" name="text"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="image">Article image</label>
                                                    <input type="text" class="form-control" id="image" placeholder="Enter article image url" name="image">
                                                </div>
                                                <div class="form-group">
                                                    <label for="author">Author</label>
                                                    <input type="text" class="form-control" id="author" placeholder="Enter author name" name="author">
                                                </div>
                                                <div class="form-group">
                                                    <label for="author-image">Author image</label>
                                                    <input type="text" class="form-control" id="author-image" placeholder="Enter author profile image url" name="author-image">
                                                </div>
                                                <div class="form-group">
                                                    <label for="date">Date</label>
                                                    <input type="date" class="form-control" id="date" placeholder="Enter article posted date" name="date">
                                                </div>

                                                <button type="submit" class="btn btn-primary" name="addArticle">Add</button>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


</body>

</html>