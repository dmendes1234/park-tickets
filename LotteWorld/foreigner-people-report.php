<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['ptmsaid'] == 0)) {
    header('location:logout.php');
} else {

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Normal People Report</title>

        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/br-posjetitelja.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>
        <?php include_once('includes/header.php'); ?>
        <?php include_once('includes/navBar.php'); ?>

        <div class="container" style="margin-top:50px">
            <h4 class="header-title" style="color: blue">Between Dates Reports of Foreigner Ticket Generating</h4>

            <form method="post" name="bwdatesreport" action="foreigner-people-report-details.php">
                <div class="form-group">
                    <label>From Date</label>
                    <input type="date" id="fromdate" name="fromdate" value="" class="form-control" required="true"></div>

                <div class="form-group">
                    <label>To Date</label>
                    <input type="date" id="todate" name="todate" value="" class="form-control" required="true">

                </div>

                <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" name="submit">Submit</button>
            </form>
    </body>

    </html>

<?php }  ?>