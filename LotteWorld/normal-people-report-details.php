<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['ptmsaid'] == 0)) {
    header('location:logout.php');
} else {

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Report Details</title>

        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/br-posjetitelja.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>
        <?php include_once('includes/header.php'); ?>
        <?php include_once('includes/navBar.php'); ?>

        <?php
        $fdate = $_POST['fromdate'];
        $tdate = $_POST['todate'];

        ?>


        <div class="container" style="margin-top: 50px;">
            <h5 text-align="center" style="color:blue">Between Dates Report from <?php echo $fdate ?> to <?php echo $tdate ?> of Ticket Generating</h5>
            <table class="table table-striped">
                <thead class="bg-light text-capitalize">
                    <tr>
                        <th>S.NO</th>
                        <th>Ticket ID</th>
                        <th>Generating Ticket Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <?php
                $ret = mysqli_query($con, "select * from tblticnormal where date(PostingDate) between '$fdate' and '$tdate'");
                $cnt = 1;
                while ($row = mysqli_fetch_array($ret)) {

                ?>
                    <tbody>
                        <tr data-expanded="true">
                            <td><?php echo $cnt; ?></td>

                            <td><?php echo $row['TicketID']; ?></td>
                            <td><?php echo $row['PostingDate']; ?></td>
                            <td><a href="view-normal-ticket.php?viewid=<?php echo $row['ID']; ?>">View</a>
                        </tr>
                    <?php
                    $cnt = $cnt + 1;
                } ?>
                    </tbody>
            </table>
        </div>
    </body>

    </html>
<?php }  ?>