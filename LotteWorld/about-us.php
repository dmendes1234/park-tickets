<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About us</title>

    <link rel="stylesheet" href="css/main.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <style>
        * {
            box-sizing: border-box;
        }

        body {
            margin: 0;
            font-family: Arial;
            font-size: 17px;
        }

        #myVideo {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
        }

        .content {
            position: fixed;
            bottom: 0;
            background: rgba(0, 0, 0, 0.5);
            color: #f1f1f1;
            width: 100%;
            padding: 20px;
        }

        #myBtn {
            width: 200px;
            font-size: 18px;
            padding: 10px;
            border: none;
            background: #000;
            color: #fff;
            cursor: pointer;
        }

        #myBtn:hover {
            background: #ddd;
            color: black;
        }
    </style>

</head>

<body>
    <video autoplay loop id="myVideo">
        <source src="video.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
    <?php include_once('includes/guest-navBar.php'); ?>

    <div class="content">
        <h1>About <span style="color:red">Lotte World</span></h1>
        <h3>Theme Parks</h3>
        <p class="about-paddingB">The Lotte World offers 2 world-class theme parks—Lotte Park in Barcelona and Lotte Madrid Adventure Park—each with its own unique attractions, shows and restaurants. Which attractions are right for you? What should you wear? Which dining locations are accessible to Guests with wheelchairs? How do you order tickets online? What kind of personal items are not permitted inside the theme parks? Find answers to frequently asked questions. Begin by checking out what each theme park has to offer below.</p>
        <p>We are also orginizing a lot of various festivals during each time of the year. For updates check our news feed.</p>
        <button id="myBtn" onclick="myFunction()">Pause the video</button>
    </div>

    <script>
        var video = document.getElementById("myVideo");
        var btn = document.getElementById("myBtn");

        function myFunction() {
            if (video.paused) {
                video.play();
                btn.innerHTML = "Pause the video";
            } else {
                video.pause();
                btn.innerHTML = "Play the video";
            }
        }
    </script>
</body>

</html>